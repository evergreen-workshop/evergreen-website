import * as React from "react"
import { Grommet, Box, Footer, Text } from "grommet"
import { hpe } from "grommet-theme-hpe"
import PropTypes from "prop-types"
import PageHeader from "./header"

const Layout = ({ children }) => {
	return (
		<Grommet theme={hpe} full>
			<Box fill>
				<PageHeader siteTitle="Unnamed Auto Detailing" />
				<Box direction="row" flex>
					<Box flex align="center">
						{children}
					</Box>
				</Box>
			</Box>
			<Footer background="background-back" justify="center" pad="small">
				<Text textAlign="center">
					© {new Date().getFullYear()} - Unnamed Auto Detailing, LLC
				</Text>
			</Footer>
		</Grommet>
	)
}

Layout.propTypes = {
	children: PropTypes.node.isRequired,
	pageTitle: PropTypes.string.isRequired,
}

export default Layout
