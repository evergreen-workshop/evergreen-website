import React from "react"
import { Anchor, Box, Button, Heading, Menu, ResponsiveContext } from "grommet"
import { HpeLabs, Menu as MenuIcon } from "grommet-icons"

const PageHeader = ({ siteTitle }) => {
	return (
		<Box 
			background="blue!" 
			direction="row"
			height="xxsmall"
			align="center">
			<Box fill align="center" direction="row">
				<Anchor
					href="/"
					icon={<HpeLabs color="brand" />}
					label={
						<Heading truncate={true} level="2" size="small">
							{siteTitle}
						</Heading>
					}
					style={{
						textDecoration: `none`
					}}
				/>
			</Box>
			<ResponsiveContext.Consumer>
				{(size) => 
					size === "xsmall" || size === "small" ? (
						<Box justify="end">
							<Menu
								dropProps={{ align: { top: "bottom", right: "right" } }}
								icon={<MenuIcon color="brand" />}
								items={[
									{
										label: <Box pad="small">Home</Box>,
										href: "/",
									},
									{
										label: <Box pad="small">Contact</Box>,
										href: "/contact",
									},
									{
										label: <Box pad="small">Projects</Box>,
										href: "/projects",
									},
								]}
							/>
						</Box>
					) : (
						<Box fill justify="end" direction="row">
							<Anchor href="/"><Button label="Home" fill="vertical"/></Anchor>
							<Anchor href="/contact"><Button label="Contact" fill="vertical"/></Anchor>
							<Anchor href="/projects"><Button label="Projects" fill="vertical"/></Anchor>
						</Box>
					)
				}
			</ResponsiveContext.Consumer>
		</Box>
	)
}

export default PageHeader
