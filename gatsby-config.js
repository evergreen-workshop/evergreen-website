module.exports = {
  siteMetadata: {
    siteUrl: `https://evergreenworkshop.net`
  },
  plugins: [
    'gatsby-plugin-image',
    'gatsby-plugin-sharp',
    'gatsby-plugin-styled-components',
    'gatsby-transformer-sharp'
  ]
};